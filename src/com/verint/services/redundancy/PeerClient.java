package com.verint.services.redundancy;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class PeerClient {
	private int m_port;
	private int m_wait;
	private String m_host;

	public PeerClient(int port, String host, int wait) {
		this.m_port = port;
		this.m_host = host;
		this.m_wait = wait;
	}
	public void run() {
		try {
			while (true){
				DatagramSocket socket = new DatagramSocket();
	
				// send request
				byte[] buf = new byte[256];
				String dString = "ping";
				buf = dString.getBytes();
				InetAddress address;
	
				address = InetAddress.getByName(m_host);
				DatagramPacket packet = new DatagramPacket(buf, buf.length,
						address, 4445);
				socket.send(packet);
				// get response
				packet = new DatagramPacket(buf, buf.length);
				socket.setSoTimeout(1000);
				System.out.println("send:" +dString +" to:" +address);
				try {
					socket.receive(packet);
					RedundancyManager.setState(address.getHostName(), true);
				} catch (SocketTimeoutException se) {
					System.out.println("Exception waiting for socket");
					//Runner.setMaster(address.getHostName(), false);
					RedundancyManager.setState(address.getHostName(), false);
					
				}
				// display response
				String received = new String(packet.getData(), 0,
						packet.getLength());
				socket.close();
				//Thread.currentThread();
				Thread.sleep(m_wait);
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		**/
	}

}
