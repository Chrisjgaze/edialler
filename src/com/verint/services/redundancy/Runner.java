package com.verint.services.redundancy;

import java.util.HashMap;
import java.util.Map;

import com.verint.services.eQConnect.EQCServlet;
import com.verint.services.eQConnect.TCPServer;
import com.verint.services.model.CallTracker;
import com.verint.services.recorders.RecorderParser;
import com.verint.services.utils.SqlConn;


public class Runner {

	public final static RedundancyManager rm = new RedundancyManager();
	public final static RecorderParser m_rp = new RecorderParser();
	public final static CallTracker m_ct = new CallTracker();
	//public final static EQCServer m_eQCS = new EQCServer();
	public final static EQCServlet m_eQCs = new EQCServlet();
	//public final static TCPServer m_tcp = new TCPServer(3031);
	public final static SqlConn m_sql = new SqlConn(); 
	
	
	public static void main(String[] args) {
		m_sql.getConnection("TOKWS54001\\TK_MS_VRA_PRD2", "ewcode", "EW03081966", "EWareCalls");
		//starttime,endtime,DDI,PhantomDDI,Digits,direction,DDILabelId,lineId,lineNameId
		//vertconfigid,starttime,endtime,callid
		//Map<String, String> m_attrs = new HashMap<String, String>();
		
		//int i = m_sql.updateCalls(m_attrs);
		//System.out.println("Got:" +i);
		//m_sql.updateConns(m_attrs);
		//System.out.println("2423H1".substring(5,6));
		
	}
	
	
	public static void setMaster(String host, boolean status) {
		System.out.println("Setting master to false " +host);
	}

}
