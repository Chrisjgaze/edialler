package com.verint.services.redundancy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

public class RedundancyManager {

	private String m_host = "";
	private String m_peerHost = "";
	private int m_port = 29530;
	private int m_peerPort = 29530;
	private static Type m_type = Type.StandAlone;
	private static Type m_peerType = Type.NonExistant;
	public static State m_state = State.Master;
	private static State m_peerState = State.Unknown;

	public RedundancyManager() {
		try {
			System.out.println("Starting Redundancy Manager");
			Properties m_props = new Properties();
			m_props.load(new FileInputStream("config.properties"));
			
			this.m_host = normaliseHostAddress(java.net.InetAddress.getLocalHost().getHostName());
			if ("Primary".equalsIgnoreCase(m_props.getProperty("type"))) {
				this.m_type = Type.Primary;
				this.m_state = State.Master;
			} else if ("Secondary".equalsIgnoreCase(m_props.getProperty("type"))) {
				this.m_type = Type.Secondary;
				this.m_state = State.Slave;
			}
			this.m_peerHost = normaliseHostAddress(m_props.getProperty("peerhost"));
			this.m_peerPort = Integer.parseInt(m_props.getProperty("peerport"));
			if ("Primary".equalsIgnoreCase(m_props.getProperty("peertype"))) {
				this.m_peerType = Type.Primary;
				this.m_peerState = State.Master;
			} else if ("Secondary".equalsIgnoreCase(m_props.getProperty("peertype"))) {
				this.m_peerType = Type.Secondary;
				this.m_peerState = State.Slave;
			}
			
			Runnable serverThread = new Thread() {
				  public void run() {
					try {
						PeerServer ps = new PeerServer("m_host");
						ps.run();
					} catch (IOException e) {
						e.printStackTrace();
					}
				  }
			};
			Thread runServer = new Thread(serverThread);
			runServer.start();
			if (m_peerHost.length() < 0) return;
			final PeerClient pc = new PeerClient(4445,m_peerHost,5000);
			Runnable clientThread = new Thread() {
				public void run() {
					pc.run();
				}
			};
			Thread runClient = new Thread(clientThread);
			runClient.start();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String normaliseHostAddress(String hostNameOrIP) {
		String ret = null;
		try {
			InetAddress ia = InetAddress.getByName(hostNameOrIP);
			ret = ia.getHostName();
		} catch (UnknownHostException e) {
			// m_log.warn("Cannot reverse peer host <%s> to host name. Will use it 'as-is'", hostNameOrIP);
			ret = hostNameOrIP;
		}
		return ret;
	}
	public static void setState(String host, boolean status) {
		if (status) {
			if (Type.Primary == m_peerType) {
				m_peerState = State.Master;
				m_state = State.Slave;
				System.out.println("Demote this server to Slave");
			} else {
				m_state = State.Master;
				m_peerState = State.Slave;
			}
		}
		else {
			if (Type.Primary == m_type) {
				System.out.println("Set state for:" +host + " to:" +State.Unknown);
				m_peerState = State.Unknown;
				if (State.Master != m_state){
					System.out.println("Promote this server to Master");
					m_state = State.Master;
				}
			} else {
				System.out.println("Set state for:" +host + " to:" +State.Unknown);
				m_peerState = State.Unknown;
				if (State.Master != m_state){
					System.out.println("Promote this server to Master");
					m_state = State.Master;
				}
			}
		}
		System.out.println("This server is set to:" +m_state + " peer server is set to:" +m_peerState);
		
	}
	public boolean isMaster(){
		if (State.Master == m_state)
			return true;
		return false;
	}
	public static enum Type {
		NonExistant, StandAlone, Primary, Secondary;
	}
	 public static enum State {
	    Unknown("Disconnected"), 
	    Master("Master"), 
	    Slave("Slave");
	    private String m_display;
	    private String m_secondaryDisplay;
	    private String m_backToPrimaryDisabledDisplay;

	    private State(String display) { this(display, display, display); } 
	    private State(String display, String backToPrimaryDisabledDisplay, String secondaryDisplay) {
	      this.m_display = display;
	      this.m_secondaryDisplay = secondaryDisplay;
	      this.m_backToPrimaryDisabledDisplay = backToPrimaryDisabledDisplay;
	    }
	  }

}
