package com.verint.services.redundancy;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class PeerServer {

	protected DatagramSocket socket = null;
	protected boolean moreQuotes = true;
	boolean isConnected = false;

	public PeerServer() throws IOException {
		this("QuoteServerThread");
	}

	public PeerServer(String name) throws IOException {
		//super(name);
		socket = new DatagramSocket(4445);
	}
	
	public void run() {
		while (moreQuotes) {
			try {
				byte[] buf = new byte[256]; // receive request
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				socket.receive(packet); // figure out response
				String dString = "pong";
				isConnected = true;
				buf = dString.getBytes(); // send the response to the client at
											// "address" and "port"
				InetAddress address = packet.getAddress();
				byte[] rec = packet.getData();
				String sent = new String(rec);
				System.out.println(address +":" + sent);
				System.out.println( "reply to" +address +":" +dString);
				int port = packet.getPort();
				packet = new DatagramPacket(buf, buf.length, address, port);
				socket.send(packet);
			} catch (IOException e) {
				e.printStackTrace();
				moreQuotes = false;
			}
		}
		socket.close();
	}
}
