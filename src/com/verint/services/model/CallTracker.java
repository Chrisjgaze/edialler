package com.verint.services.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.verint.services.recorders.RecorderManager;
import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;

public class CallTracker {
	private static Map<String, Recording> m_calls = new HashMap<String, Recording>();
	private static Map<String, String> m_extns = new HashMap<String, String>();
	private static Map<String, Integer> m_recorders = new HashMap<String, Integer>();
	private static Map<String, Map<String, String>> m_tagQ = new HashMap<String, Map<String,String>>();
	private static Map<String, EDiallerCall> m_edial = new HashMap<String, EDiallerCall>();
	
	private static TimerWheelCommand m_command;

	public CallTracker() {	
		m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckMaps(), 5000L, 10000L);
	}
	public static String getMapDetails() {
		try {
		String ret = "Maps contain:: m_calls:"; 
		ret = ret +m_calls.size();
		ret = ret +" m_extns:" +m_extns.size(); 
		ret = ret +" m_tagQ:" +m_tagQ.size();
		ret = ret +" m_edial:" +m_edial.size();
		System.out.println(ret);
		return ret;
		} catch (Exception e){
			System.out.println("Exception gettting Map details:" +e.getMessage());
			return "";
		}
	}

	public static void add(String s_inum, Recording recording) throws Exception {
		m_calls.put(s_inum, recording);
		if (recording.m_extn.length() >0) {
			m_extns.put(recording.m_extn,s_inum);
		}
		checkTagQ(recording.m_extn, recording);
	}
	public static void addEDial(EDiallerCall eCall) {
		System.out.println("Adding eDailler call for device:" +eCall.m_extn);
		try {
			m_edial.put(eCall.m_extn, eCall);
		} catch (Exception e) {
			System.out.println("Exception adding eDailler call" +e.getMessage());
		}
	}
	public static void removeEDial(String extn){
		m_edial.remove(extn);
	}
	public static void update(String s_inum, Recording recording) {
		try{
			if (m_calls.containsKey(s_inum)) {
				m_calls.remove(s_inum);
				m_calls.put(s_inum, recording);
				if (recording.m_extn.length() >0) {
					m_extns.remove(recording.m_extn);
					m_extns.put(recording.m_extn,s_inum);
				}
				try {
				if (recording.getAttributeValue("XML").contains("frameworktagged")) {
					System.out.println("Frameworktagged - update on stitchinum, overwrite");
					checkTagQ(recording.m_extn, recording);
				} else {
					System.out.println("update has eDialler");
				}
				} catch (Exception e) {
					System.out.println("Call is CSTDM - no XML to parse");
				}
			}
		} catch (Exception e) {
			System.out.println("Exception updating call" +e.getMessage());
		}
	}
	public static void remove(String s_inum) {
		if (m_calls.containsKey(s_inum)) {
			System.out.println("Remove call - found in tracker");
			Recording m_recording = m_calls.get(s_inum);
			m_extns.remove(m_recording.m_extn);
			m_calls.remove(s_inum);
			reTag(m_recording);
		}else {
			System.out.println("Tracker does not contain call for inum:" +s_inum);
		}
	}
	public static Recording getRecording(String inum) {
		Recording m_recording = m_calls.get(inum);
		return m_recording;
	}
	public static Recording getByExtn(String ext) {
		Recording m_recording = m_calls.get(m_extns.get(ext));
		return m_recording;
	}
	public static EDiallerCall getEDiallerCall(String ext) {
		return m_edial.get(ext);
	}
	public static void addTagQ(String extn, Map<String, String> tags) {
		m_tagQ.put(extn, tags);
	}
	private static void reTag(Recording rec) {
		System.out.println("Re-Tagging call as e-Dialler");
		EDiallerCall eCall = m_edial.get(rec.m_extn);
		String s_tag = "<stitchid>" +rec.m_stitchid +"</stitchid>"; //<udf5>e-Dialler Call</udf5>";
		if (null != eCall){
			System.out.println("Re-Tagging with eDailler events");
			s_tag = s_tag +"<udf8>" +eCall.m_digits +"</udf8>";
			rec.m_iprec.sendIPTagMessage(rec.m_inum, s_tag);
		} else {
			System.out.println("Re-Tagging did not find eDailler call.");
		}
	}
	private static void checkTagQ(String extn, Recording rec) {
		if (m_edial.containsKey(rec.m_extn)) {
			System.out.println("Got eDialler call:");
			EDiallerCall eCall = m_edial.get(rec.m_extn);
			if (null == eCall.m_inum) {
				System.out.println("Setting eCall inum:" +rec.m_inum);
				eCall.m_inum = rec.m_inum;
			}
			rec.m_stitchid = eCall.m_inum;
			System.out.println("Tagging call as e-Dialler");
			String s_tag = "<stitchid>" +rec.m_stitchid +"</stitchid>"; //<udf5>e-Dialler Call</udf5>";
			s_tag = s_tag +"<udf8>" +eCall.m_digits +"</udf8>";
			rec.m_iprec.sendIPTagMessage(rec.m_inum, s_tag);
			if (m_calls.containsKey(rec.m_inum)) {
				m_calls.remove(rec.m_inum);
				m_calls.put(rec.m_inum, rec);
			} else {
				m_calls.put(rec.m_inum, rec);
			}
		}
		if (m_tagQ.containsKey(extn)){
			System.out.println("checkTagQ has event for:" +extn);
			Map<String, String> tag = m_tagQ.get(extn);
			tag.put("stitchid", rec.m_inum);
			String s_tag = "<stitchid>" +rec.m_stitchid +"</stitchid>"; //<udf5>e-Dialler Call</udf5>";
			//rec.m_iprec.sendIPTagMessage(rec.m_inum, s_tag);
		}
	}
	public List<Recording> getAll() {
		List<Recording> m_list = new ArrayList<Recording>();
		for(String inum : m_calls.keySet()) {
			m_list.add(m_calls.get(inum));
		}
		return m_list;
	}
	public String GetRecorderStatus() {
		String s_ret = "";
		for (String s_serial : m_recorders.keySet()) {
			s_ret = s_ret
					+ "Recorder:" 
					+ RecorderManager.GetRecorderDetails(s_serial)
					+ " has:"
					+ m_recorders.get(s_serial)
					+ " calls<br>";
		}
		return s_ret;
	}
	public void Routiner() {
		m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckMaps(), 5000L, 10000L);
	}
	private class CheckMaps implements Runnable {
		private CheckMaps() {	}
		public void run() {
			try {
				System.out.println("Heap utilization statistics [MB]");
				int mb = 1024*1024;
				//Getting the runtime reference from system
				Runtime runtime = Runtime.getRuntime();
				long i_used = (runtime.totalMemory() - runtime.freeMemory()) / mb;
				long i_free = runtime.freeMemory() / mb;
				long i_total = runtime.totalMemory() / mb;
				long i_max = runtime.maxMemory() / mb;
				System.out.println(String.format("Used:%s, Free:%s, Total:%s, Max:%s",i_used, i_free, i_total, i_max));
				System.out.println("Checking map information");
				CallTracker.getMapDetails();
				//System.out.println(String.format("Maps contain:: ", CallTracker.getMapDetails()));
			}
			catch (Exception e){
				System.out.println("Exception in CheckMaps:" +e.getMessage());

			}
		}
	}
}
