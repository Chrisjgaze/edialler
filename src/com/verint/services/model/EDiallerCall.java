package com.verint.services.model;

import java.util.HashMap;
import java.util.Map;

public class EDiallerCall {
	public String m_extn;
	public String m_callid;
	public String m_inum;
	public String m_digits;
	public Map<String, String> m_attrs = new HashMap<String, String>();
	
	public EDiallerCall() { }
	
	public void DisplayTagDetails() {
		for (String s : m_attrs.keySet()) {
			System.out.println("Tag Key:" +s +" with value:" +m_attrs.get(s));
		}
	}
	
	public void addTags(String s_key, String s_value) {
		try {
			m_attrs.put(s_key, s_value);
		} catch (Exception e) {
			System.out.println("Exception adding tags:" +e.getMessage());
		}
	}
	public String getTag(String s_key) {
		return m_attrs.get(s_key);
	}
	
	public void endCall() {
		
	}

}
