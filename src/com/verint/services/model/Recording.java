package com.verint.services.model;


import java.util.Date;
import java.util.Map;

import com.verint.services.recorders.IPRole;
import com.verint.services.recorders.TdmRole;
import com.witness.common.util.DirtyOldMap;
import com.witness.common.util.DirtyOldShortHashMap;

public class Recording {
	public String m_inum;
	public String m_extn;
	public String m_callID;
	public String m_stitchid;
	public Date m_startedat;
	public String m_ip;
	private DirtyOldMap<String, String> m_attributes = new DirtyOldShortHashMap();
	public IPRole m_iprec;
	public TdmRole m_tdm;
	public TagDetails m_tagDetails;
	
	public Recording(String inum) {
		this.m_inum = inum;
		this.m_startedat = new Date(System.currentTimeMillis());
		m_tagDetails = new TagDetails();
	}
	public void addAttribute(String key, String value) {
		if ((null == key) || (0 == key.length()) || (null == value) || (0 == value.length())) {
			return;
		}
		this.m_attributes.put(key, value);
		}

	public void addAttributes(Map<String, String> attributes) {
		try{
			for (Map.Entry entry : attributes.entrySet())
				addAttribute((String)entry.getKey(), (String)entry.getValue()); 
		} catch (Exception e) {
			System.out.println("Exception adding attributes:" +e.getMessage());
		}
	}
	public Recording getRecording() {
		return null;
	}
	public String getAttribute(String name){
		String ret = null;
		ret = m_attributes.get(name);
		return ret;
	}
	public Map<String, String> getChangedAttributes() {
		return this.m_attributes.getDirtyMap();
	}
	public void clearChangedAttributes() { this.m_attributes.clearDirtySet(); } 
	public String getAttributeValue(String key) {
		return (String)this.m_attributes.get(key);
	}

	public String toString() {
		String ret= null;
		ret = this.m_extn +" : ";
		for (Map.Entry entry : m_attributes.entrySet())
			ret +=entry.getKey() +" " +entry.getValue() + " ";
		return ret;
	}
}
