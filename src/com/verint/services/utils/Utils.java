package com.verint.services.utils;

public class Utils {

	  public String[] cSStoArray(String aString){
		    String[] splittArray = null;
		    if (aString != null && !aString.equalsIgnoreCase("")){
		         splittArray = aString.split(",");
		    }
		    return splittArray;
		}
	  public String[] trimString(String[] aString){
       if(aString != null)
       {
      	 for (int i = 0; i < aString.length; i++) {
      		 aString[i] = aString[i].trim();
			}
       }
       return aString;
	  }
}
