package com.verint.services.utils;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public class SqlConn {
	private java.sql.Connection con = null;
	private static String strConnString;
	public String strConStatus;
	private String strConnector;

	public SqlConn() {
		this.initialize();
	}
	public java.sql.Connection getConnection(String dbServer, String user,
			String pass, String dbName) {
		try {
			// Note: this class name changes for ms sql server 2000 thats it
			// It has to match the JDBC library that goes with ms sql 2000
			// Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			strConnector = getConnectionUrl(dbServer, user, pass, dbName);
			DriverManager.setLoginTimeout(5);
			System.out
			.println("Class attempted. Connecting to DB with ConString '"
					+ strConnector
					+ "'. Default timeout is '"
					+ DriverManager.getLoginTimeout() + "'.");
			con = DriverManager.getConnection(strConnector);
			System.out.println("Connection processed.");
			if (con != null) {
				System.out.println("Connection to database successful!");
				strConStatus = "Connected";
				return con;
			}
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("Exception connecting to datatbase: "
					+ e.getMessage());
			strConStatus = (e.getMessage());
		}
		return null;
	}
	private void initialize() {
		// this.con = null;
	}
	private String getConnectionUrl(String dbServer, String user, String pass,
			String dbName) {
		if ("".equalsIgnoreCase(user)) {
			//
			strConnString = "jdbc:sqlserver://" + dbServer + ";databaseName="
					+ dbName + ";integratedSecurity=true;";
			System.out.println("getConnectionURL:" + strConnString);
			System.out.println(strConnString);
		} else {
			strConnString = "jdbc:sqlserver://" + dbServer + ";user=" + user
					+ ";" + "password=" + pass + ";databaseName=" + dbName
					+ ";";
			System.out.println("getConnectionURL:" + strConnString);
			System.out.println(strConnString);
		}
		return strConnString;
	}
	public Boolean chkConn() {
		try {
			if (con.getCatalog().toString().equals("")) {
				System.out.println(con.getCatalog().toString());
				return false;
			} else
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (NullPointerException en) {
			en.printStackTrace();
			return false;
		}
	}
	public int updateCalls(String s_sql) {
		//starttime,endtime,DDI,PhantomDDI,Digits,direction,DDILabelId,lineId,lineNameId
		int ret = 0;
		String query = s_sql; //"INSERT INTO test VALUES('1234')";
		try {
			Statement stmt = con.createStatement();
			ret = stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();

			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			if (rs.next()) {
				do {
					for (int i = 1; i <= columnCount; i++) {
						String key = rs.getString(i);
						System.out.println("KEY " + i + " = " + key);
						ret = Integer.parseInt(key);
					}
				} while (rs.next());
			} else {
				System.out.println("NO KEYS WERE GENERATED.");
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {

		}
		return ret;
	}

	public void updateConns(Map<String, String> m_attrs) {
		String query = "INSERT INTO test VALUES('1234'); SELECT SCOPE_IDENTITY()";
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
		} catch (Exception e) {

		}
	}
	public ResultSet conQuery(String strSQL) {
		// conLocal = this.getLocalConnection();
		System.out.println("Run ConQuery");
		Statement SelectMaxInum = null;
		try {
			SelectMaxInum = con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ResultSet rltInum = SelectMaxInum.executeQuery(strSQL);
			return rltInum;

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}
	public Boolean dbExecSP(String strSQL) throws SQLException {
		con.createStatement().execute(strSQL);
		return true;
	}

	public void closeConnection() {
		try {
			if (con != null)
				con.close();
			con = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
