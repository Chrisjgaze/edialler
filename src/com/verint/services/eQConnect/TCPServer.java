package com.verint.services.eQConnect;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

import com.verint.services.model.CallTracker;
import com.verint.services.model.Recording;
import com.verint.services.nio.handlers.Acceptor;
import com.verint.services.nio.handlers.AcceptorListener;
import com.verint.services.nio.handlers.PacketChannel;
import com.verint.services.nio.handlers.PacketChannelListener;
import com.verint.services.nio.handlers.SimpleProtocolDecoder;
import com.verint.services.nio.io.SelectorThread;
import com.verint.services.utils.Utils;


public class TCPServer implements AcceptorListener, PacketChannelListener {
	
	private static SelectorThread st = null;
	private static Utils m_utils = new Utils();
	
	public TCPServer(int port){
		m_utils = new Utils();
	    try {
		    // Starting a thread for listening to socket communication
		    st = new SelectorThread();
		    Acceptor acceptor = new Acceptor(port, st, this);
		    acceptor.openServerSocket();
		    System.out.println("simware is Listening to sockets communication on port: " + port);
	    }
	    catch(IOException e)
	    {
	    	System.out.println("Exception listening on port:" + port +" " +e.getMessage());
	    }
	}
	
	public void socketConnected(Acceptor acceptor, SocketChannel sc) {
	    try {
	    	sc.socket().setReceiveBufferSize(2*1024);
	    	sc.socket().setSendBufferSize(2*1024);
	    	PacketChannel pc = new PacketChannel(
	    			sc,
	    			st,
	    			new SimpleProtocolDecoder(),
	    			this);
	    	pc.resumeReading();
	    } 
	    catch (IOException e) {
	    	e.printStackTrace();
	    }
	  }

	  public void socketError(Acceptor acceptor, Exception ex) {
		  System.out.println("["+ acceptor + "] Error: " + ex.getMessage());
	  }

	  public void packetArrived(PacketChannel pc, ByteBuffer pckt) {
		Charset charset = Charset.forName("ISO-8859-1");//
		CharsetDecoder decoder = charset.newDecoder();
		ByteBuffer pckt1;
		pckt1 = pckt.duplicate();
	    ByteBuffer buf = pckt1;
	    CharBuffer cbuf;
	    String cbufStr = "";
		pc.clearBuffer();
		try {
			cbuf = decoder.decode(buf);
			cbufStr = cbuf.toString();
			cbuf = null;
			pckt1 = null;
			String aString[];
			aString = m_utils.cSStoArray(cbufStr);
			aString = m_utils.trimString(aString);
			//Message msg = new Message(cbufStr);
			System.out.println("aStirng:" +aString[1]);
			if ("split".equalsIgnoreCase(aString[1])) {
				System.out.println("Break recording");
				Recording m_rec = CallTracker.getByExtn("36463");
				//m_rec.m_iprec.breakCall(m_rec.m_callID, m_rec.m_extn, m_rec.m_inum);
			}
			if ("stop".equalsIgnoreCase(aString[1])) {
				CallTracker.removeEDial("36463");
			}
		}
		catch (Exception e) {
			System.out.println("Exception on packetArrived:" +e.getMessage());
		}
	  }

	public void packetSent(PacketChannel pc, ByteBuffer pckt) {
	    try {
	    	pc.resumeReading();
		} catch (Exception e) {
			System.out.println("Exception reading packet:" +e.getMessage());
		}
	}

	public void socketException(PacketChannel pc, Exception ex) {
		// TODO Auto-generated method stub
		
	}

	public void socketDisconnected(PacketChannel pc) {
		// TODO Auto-generated method stub
		
	}
}
