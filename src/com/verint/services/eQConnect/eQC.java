package com.verint.services.eQConnect;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.verint.services.model.CallTracker;
import com.verint.services.model.EDiallerCall;
import com.verint.services.model.Recording;
import com.verint.services.redundancy.RedundancyManager;
import com.verint.services.redundancy.RedundancyManager.State;
import com.verint.services.redundancy.Runner;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class eQC extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(final HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
		httpServletResponse.setContentType("text/plain");
		System.out.println(httpServletRequest.getQueryString());
		java.lang.Runnable handleEvent = new java.lang.Thread() {
			public void run() {
				handleRequest(httpServletRequest.getQueryString());
			}
		};
		java.lang.Thread doEvent = new java.lang.Thread(handleEvent);
		doEvent.start();
		PrintWriter out = httpServletResponse.getWriter();
		out.println("eQCServer received request");
		out.close();
	}
	private void handleRequest(String s_req) {
		try {
			eQCEvent m_event = new eQCEvent();
			String s_del = "=";
			s_req = "&" + s_req;
			String s_attrKey = null;
			String s_attrValue = null;
			HashMap<String, String> m_kvps = new HashMap<String, String>();
			StringTokenizer st = new StringTokenizer(s_req, "&;");
			while (st.hasMoreTokens()) {
				String kvp[] = st.nextToken().split(s_del);
				// System.out.println(kvp.length);
				if (kvp.length == 2) {
					System.out.println(kvp[0] + "\t" + kvp[1]);
					if ("interface".equalsIgnoreCase(kvp[0])) {
						m_event.m_interface = kvp[1];
					}
					if ("method".equalsIgnoreCase(kvp[0])) {
						m_event.m_method = kvp[1];
					}
					if ("contactevent".equalsIgnoreCase(kvp[0])) {
						m_event.m_contactevent = kvp[1];
					}
					if ("device.device".equalsIgnoreCase(kvp[0])) {
						m_event.m_device = kvp[1];
					}
					if ("attribute.key".equalsIgnoreCase(kvp[0])) {
						s_attrKey = kvp[1];
					}
					if ("attribute.value".equalsIgnoreCase(kvp[0])) {
						s_attrValue = kvp[1];
						m_event.m_attrs.put(s_attrKey, s_attrValue);
						s_attrKey = null;
						s_attrValue = null;
					}
				}
			}
			m_event.displayEvent();
			if ("Connected".equalsIgnoreCase(m_event.m_contactevent)) {
				System.out.println("Process Connected Event");
				if (RedundancyManager.m_state == State.Master) {
					System.out.println("This is Master");
					processConnected(m_event);
				}
			}
			if ("Disconnected".equalsIgnoreCase(m_event.m_contactevent)) {
				System.out.println("Process Disconnected Event");
				if (RedundancyManager.m_state == State.Master) {
					System.out.println("This is Master");
					processDisconnect(m_event);
				}
			}
		} catch (Exception e) {
			System.out.println("Exception in eQC:" + e.getMessage());
		}
	}

	private void processConnected(eQCEvent m_event) {
		Recording m_rec = CallTracker.getByExtn(m_event.m_device);
		// check to see if there is a call there already
		EDiallerCall eCall = CallTracker.getEDiallerCall(m_event.m_device);
		if (null == eCall) {
			// its a new call
		} else {
			// detected exsisting edial call, terminate first
			System.out.println("Found exsisting eDail call, terminate first");
			processDisconnect(m_event);
		}
		// check to see if call is found
		if (null != m_rec) {
			// check to see if call is tdm or ip
			if (null != m_rec.m_iprec) {
				m_rec.m_iprec.breakCall(m_rec.m_callID, m_rec.m_extn, m_rec.m_inum, m_event);
			}
			if (null != m_rec.m_tdm) {
				// break tdm call
				System.out.println("Break ITS call");
				m_rec.m_tdm.breakCall(m_rec.m_callID, m_rec.m_extn, m_rec.m_inum, m_event);
			}
		} else {
			System.out.println("No call found on device:" + m_event.m_device);
		}
	}

	private void processDisconnect(eQCEvent m_event) {
		// on disconnect just need end time
		try {
			TDMDisconnect(m_event);
			Recording m_rec = CallTracker.getByExtn(m_event.m_device);

			if (null == m_rec) {
				System.out.println("No current recordings found for disconnect");
				// TDMDisconnect(m_event);
			} else {

				if (null != m_rec.m_tdm) {
					// m_rec.m_tdm.breakCall(m_rec.m_callID, m_rec.m_extn,
					// m_rec.m_inum,
					// m_event);
					// m_rec.m_tdm.breakCall(m_rec.m_callID, m_rec.m_extn,
					// m_rec.m_inum, m_event);
				} else {
					System.out.println("Got split, check for IP Calls");
				}
			}
			CallTracker.removeEDial(m_event.m_device);
		} catch (Exception e) {
			System.out.println("Exception processing disconnect:" + e.getMessage());
			CallTracker.removeEDial(m_event.m_device);
		}
	}
	private void TDMDisconnect(eQCEvent m_event) {
		try {
			System.out.println("TDM disconnect event for device:" + m_event.m_device);
			EDiallerCall eCall = CallTracker.getEDiallerCall(m_event.m_device);
			if (null != eCall) {
				System.out.println("Found eDiallerCall for device:" + m_event.m_device);
				Date d_start = new Date(System.currentTimeMillis());
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				eCall.addTags("endtime", sf.format(d_start));
				eCall.DisplayTagDetails();

				String s_sql = " INSERT INTO Calls (starttime,endtime,DDI,PhantomDDI,Digits,direction,DDILabelId,lineId,lineNameId)" + "VALUES(" + "'" + eCall.getTag("starttime") + "'" + ", '"
						+ eCall.getTag("endtime") + "'" + ", " + eCall.getTag("DDI") + " ," + eCall.getTag("PhantomDDI") + ", " + eCall.getTag("num_called") + ", 2" + ", "
						+ eCall.getTag("DDILabelId") + ", " + eCall.getTag("lineId") + ", " + eCall.getTag("lineNameId") + ");";
				System.out.println("SQL:" + s_sql);
				int i_callId = Runner.m_sql.updateCalls(s_sql);

				s_sql = "INSERT INTO conns(vertconfigid,starttime,endtime,callid) VALUES(" + eCall.getTag("VertConfigId") + ", '" + eCall.getTag("starttime") + "'" + ", '" + eCall.getTag("endtime")
						+ "'" + ", " + i_callId + ");";
				System.out.println("SQL:" + s_sql);

				i_callId = Runner.m_sql.updateCalls(s_sql);

			} else {
				System.out.println("Did not find eDailler call for device:" + m_event.m_device);
			}
		} catch (Exception e) {
			System.out.println("Exception caught processing TDMDisconnect:" + e.getMessage());
		}
	}
}
