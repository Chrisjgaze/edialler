package com.verint.services.eQConnect;

import java.util.HashMap;
import java.util.Map;

public class eQCEvent {
	public String m_interface;
	public String m_method;
	public String m_contactevent;
	public String m_device;
	public String m_agent;
	public String m_digits;
	public Map<String, String> m_attrs = new HashMap<String, String>();

	public void displayEvent() {
		String s_disp = "eQCEvent:" +m_interface +" ; " +m_method +" ; " + m_contactevent +" ; " + m_device;
		String s_attrs = " ; ";
		for (Map.Entry entry : m_attrs.entrySet()) {
			s_attrs = s_attrs +"attribute.key=" +entry.getKey();
			s_attrs = s_attrs + " ; attribute.value=" +entry.getValue();
		}
		System.out.println(s_disp +s_attrs);
	}
}
