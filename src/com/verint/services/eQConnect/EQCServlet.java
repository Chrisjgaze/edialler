package com.verint.services.eQConnect;

import java.io.IOException;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.ServletHttpContext;

public class EQCServlet {
	// We will create our server running at http://localhost:8070
	public EQCServlet() {
		try {
			Server m_server = new Server();
			m_server.addListener(":3030");

			// We will deploy our servlet to the server at the path '/'
			// it will be available at http://localhost:8070
			ServletHttpContext context = (ServletHttpContext) m_server
					.getContext("/");
			context.addServlet("/", "com.verint.services.eQConnect.eQC");

			m_server.start();
			System.out.println("started");

		} catch (IOException e) {
			System.out.println("Exception in EQCServer" +e.getMessage());
		} catch (Exception e) {
			System.out.println("Exception in EQCServer" +e.getMessage());
		}
	}
}
