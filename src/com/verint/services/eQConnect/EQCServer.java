package com.verint.services.eQConnect;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.verint.services.model.CallTracker;
import com.verint.services.model.Recording;
import com.witness.common.wdls.WitLogger;


public class EQCServer {
	private final WitLogger m_log = WitLogger.create("eQCServer");
	public EQCServer() {
		try {
			m_log.debugHigh("Start eQCServer on port 3030");
			InetSocketAddress addr = new InetSocketAddress(3030);
			HttpServer server = HttpServer.create(addr, 0);
			server.createContext("/", new MyHandler());
			server.setExecutor(Executors.newCachedThreadPool());
			server.start();
			m_log.debugHigh("Server is listening on port 3030");
		} catch (Exception e) {
			
		}
	}
}

class MyHandler implements HttpHandler {
	private final WitLogger m_log = WitLogger.create("eQCServer");
	public void handle(HttpExchange exchange) throws IOException {
		String requestMethod = exchange.getRequestMethod();
		String s_response = "";
		if (requestMethod.equalsIgnoreCase("GET")) {
			Headers responseHeaders = exchange.getResponseHeaders();
			responseHeaders.set("Content-Type", "text/html");
			responseHeaders.add("Content-type", "text/html");
			//exchange.sendResponseHeaders(200, 0);

			OutputStream responseBody = exchange.getResponseBody();
			/**
			Headers requestHeaders = exchange.getRequestHeaders();
			System.out.println(exchange.getRequestURI().toString());
			Set<String> keySet = requestHeaders.keySet();
			Iterator<String> iter = keySet.iterator();
			while (iter.hasNext()) {
				String key = iter.next();
				List<String> values = requestHeaders.get(key);
				String s = key + " = = " + values.toString() + "\n";
				//responseBody.write(s.getBytes());
			}
			**/
			if("/Status".equalsIgnoreCase(exchange.getRequestURI().toString())) {
				System.out.println("status request");
				//Dashboard m_dash = new Dashboard();
				//s_response = m_dash.Response(); 
			}
			else if ("/Agents".equalsIgnoreCase(exchange.getRequestURI().toString())) {
				//AgentDashboard m_agentdb = new AgentDashboard();
				//s_response = m_agentdb.Response();
			}
			else {
				m_log.debugHigh("Handle eQC Events");
				s_response = handleRequest(exchange.getRequestURI().toString());
			}
			System.out.println("dd:" +s_response);
			exchange.sendResponseHeaders(200, s_response.getBytes().length);
			responseBody.write(s_response.getBytes());
			responseBody.close();
		}
	}
	//http://localhost:3030/servlet/eQC6?interface=IAgentManagement&method=deliverevent&agentevent=AgentLogon&device.device=UK-CGAZE7-LP2&agent.agent=CGAZE
	//http://localhost:3030/servlet/eQC6?redirecturl=&interface=ISessionManagement&method=deliverevent&contactevent=Connected&device.device=12345&
	private String handleRequest(String s_req) {
		m_log.debugHigh("Agent Interface: %s", s_req);
		String s_del = "=";
		s_req = s_req.substring(27);
		s_req = "&" +s_req;
		HashMap<String, String> m_kvps = new HashMap<String, String>();
		StringTokenizer st = new StringTokenizer(s_req, "&;"); 
		while(st.hasMoreTokens()) { 
			String kvp[] = st.nextToken().split(s_del);  
			System.out.println(kvp[0] + "\t" + kvp[1]);
			if (kvp[0].equals("device.device")){
				m_kvps.put("pchostname", kvp[1]);
			}
			else if (kvp[0].equals("agent.agent")){
				m_kvps.put("ntlogin", kvp[1]);
				System.out.println(kvp[1]);
			}
			else {
				m_kvps.put(kvp[0], kvp[1]);
			}
		}
		if (s_req.contains("AgentLogon")){
			//NotificationServices.m_agnt.addAgent(m_kvps);
			try {
			//NotificationServices.m_eQCC.Connected(m_kvps.get("pchostname"));
			} catch (Exception e) {
				System.out.println("Exception responding to login event:" +e.getMessage());
			}
		}
		if (s_req.contains("AgentLogoff")){
			//NotificationServices.m_agnt.removeAgent(m_kvps);
		}
		if (s_req.contains("KeepAlive")){
			
		}
		if (s_req.contains("Connected")){
			//start record
			m_log.debugHigh("Connected event");
			Recording m_rec = CallTracker.getByExtn(m_kvps.get("extn"));
			if (null != m_rec) {
				//m_rec.m_iprec.breakCall(m_rec.m_callID, m_rec.m_extn);
			}
		}
		return m_kvps.get("method");
	}
}
