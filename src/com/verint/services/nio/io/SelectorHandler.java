package com.verint.services.nio.io;

/**
 * Marker interface for classes that are able to handle I/O events
 * raised by the SelectorThread class. This interface should not
 * be implemented directly. Instead, use one of the sub-interfaces
 * define specific functionality for a particular event.
 *  
 */
public interface SelectorHandler {
}
