package com.verint.services.nio.handlers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

import com.verint.services.nio.io.ProtocolDecoder;

/**
 * Decoder for an imaginary protocol. The packets of this protocol have the
 * following (very simple) format:
 * 
 * <STX><Data><ETX>
 * 
 * where
 * 
 * - <STX> is ascii 02.
 * - <ETX> is ascii 03.
 * - <asciiData> is any valid character.
 * 
 */
final public class SimpleProtocolDecoder implements ProtocolDecoder {
	
 
	
  Charset charset = Charset.forName("ISO-8859-1");//
  CharsetDecoder decoder = charset.newDecoder();
	
  /** Initial byte of a packet. */
  public static final byte STX = 0x02;
  /** Final byte of a packet. */
  public static final byte ETX = 0x0d; 
  /** Initial byte of a packet. */
  public static final byte LF = 0x0a;
  
  /**
   * Size of the buffer used to reconstrut the packet. Should be big 
   * enough to hold an entire packet.
   */
  private final static int BUFFER_SIZE = 10*1024;
  
  
  /**
   * Holds a message that is not fully assembled. This buffer is fixed-size.
   * If it is exceed, an Exception is raised by the decode() method. 
   */
  private byte[] buffer = new byte[BUFFER_SIZE];
  /** Write position on the previous buffer. */
  private int pos = 0;
  
  public ByteBuffer decode(ByteBuffer socketBuffer) throws IOException {    
    // Reads until the buffer is empty or until a packet
    // is fully reassembled.
    
	  Integer cbufSize = null;
	  
	  while (socketBuffer.hasRemaining()) {
      // Copies into the temporary buffer
      byte b = socketBuffer.get();
      try {
    	 // if (b != LF){
    		  buffer[pos] = b;
    		  //logger.info("b=" + b);
    	  //}
      } catch (IndexOutOfBoundsException e) {
        // The buffer has a fixed limit. If this limit is reached, them
        // most likely the packet that is being read is corrupt.
        e.printStackTrace();
        throw new IOException(
            "Packet too big. Maximum size allowed: " + BUFFER_SIZE + " bytes.");
      }
      pos++;
      
      // finding the size if the message by reading the first four bytes.
      if (pos==4)
      {
          byte[] IFModuleBuffer = new byte[pos];
          System.arraycopy(buffer, 0, IFModuleBuffer, 0, pos);
          ByteBuffer IFModelPacketBuffer = ByteBuffer.wrap(IFModuleBuffer);        
          
    	  ByteBuffer pckt1;
    	  pckt1 = IFModelPacketBuffer.duplicate();
    	  ByteBuffer buf = pckt1;
    	  CharBuffer cbuf;

    	  
    	  cbuf = decoder.decode(buf);
    	  String cbufStr = cbuf.toString();
    	  

    	  try
    	  {
    		  cbufSize = new Integer(cbufStr);
    	  }
    	  catch(NumberFormatException nfe)
    	  {
    		  
    	  }
      }
      else if( cbufSize != null && pos == 4+cbufSize.intValue())
      {
    	  // you are here because we read the size bytes of the packet identified by the first four bytes
          byte[] IFModuleBuffer = new byte[pos-4];
          System.arraycopy(buffer, 4, IFModuleBuffer, 0, pos-4);
          ByteBuffer IFModulePacketBuffer = ByteBuffer.wrap(IFModuleBuffer);        
          
    	  
    	  ByteBuffer pckt1;
    	  pckt1 = IFModulePacketBuffer.duplicate();
    	  ByteBuffer buf = pckt1;

          
          CharBuffer cbuf;
    	  cbuf = decoder.decode(buf);
    	  String cbufStr = cbuf.toString();
    	  
    	  System.out.println("[" + cbufStr + "] Packet received!. Size: " + cbuf.remaining());
     
          pos = 0;
          // returning here the packet message for parsing.
          return IFModulePacketBuffer;
      }
      
      // Check if it is the final byte of a packet.
      if (b == ETX) {
    	  System.out.println("Socket communication received:");
        // The current packet is fully reassembled. Return it
        byte[] newBuffer = new byte[pos];
        System.arraycopy(buffer, 0, newBuffer, 0, pos);
        ByteBuffer packetBuffer = ByteBuffer.wrap(newBuffer);        
        pos = 0;

        return packetBuffer;
      }
    }
    // No packet was reassembled. There is not enough data. 
    // Wait for more data to arrive.
    return null;
  }
}