package com.verint.services.recorders;

import java.util.List;
import java.util.Map;

public class BaseRecorder {
	protected String m_serialNo;
	protected String m_hostname;
	protected String m_type;
	protected int m_id;
	protected int m_port = 29504;
	protected List<String> m_roles;
	
	public BaseRecorder(int id, String hostname, String serialNo, List<String> roles, Map<String, String> rec_settings) {
		System.out.println("New base Recorder id: " +id);
		this.m_id = id;
		this.m_hostname = hostname;
		this.m_serialNo = serialNo;
		this.m_roles = roles;
		for (String s : roles) {
			addRoles(s);		
		}
		for (Map.Entry entry : rec_settings.entrySet())
			if ("General_IPControlPort".equals(entry.getKey())){
				m_port = Integer.parseInt((String)entry.getValue());
			}

	}
	public boolean addRoles(String role) {
		if (("iprecorder".equalsIgnoreCase(role)) || ("ip_recorder".equalsIgnoreCase(role))) {
			IPRole ipclass = new IPRole(m_id, m_hostname);
		}
		if (("tdmrecorder".equalsIgnoreCase(role)) || ("tdm_recorder".equalsIgnoreCase(role))) {
			TdmRole tdmclass = new TdmRole(m_id, m_hostname);
		}
		return true;
	}
}
