package com.verint.services.recorders;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.verint.services.eQConnect.eQCEvent;
import com.verint.services.model.CallTracker;
import com.verint.services.model.EDiallerCall;
import com.verint.services.model.Recording;
import com.verint.services.redundancy.Runner;
import com.witness.common.NGA.IComponentRegistration;
import com.witness.common.NGA.IProcessNGAMessage;
import com.witness.common.NGA.NGAClient;
import com.witness.common.NGA.NGAConnection;
import com.witness.common.NGA.NGAMessage;
import com.witness.common.threading.AsyncTimerWheelCommand;
import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;
import com.witness.common.util.HexUtils;
import com.witness.common.wdls.WitLogger;

public class TdmRole implements IComponentRegistration, IProcessNGAMessage {
	private final WitLogger m_log = WitLogger.create("TdmRoleClass");
	private String m_hostname;
	private int m_id;
	private int m_port = 1462;
	private NGAConnection m_ngaConnection;
	private NGAClient client = null;
	private TimerWheelCommand m_command;
	private Object m_commandLock = new Object();
	private int m_commandId = 1;
	private int m_update = 1;
	private TimerWheelCommand m_ConnCommand;

	public TdmRole(int id, String hostname) {
		System.out.println("Creating TDM role for recorder id:" + id);
		this.m_id = id;
		this.m_hostname = hostname;
		start();
	}
	public boolean start() {
		this.m_log.info("Get connection to Recorder %s on port %s", m_hostname, m_port);
		if (this.client == null) {
			this.client = new NGAClient(m_hostname, m_port, 1, "IF", "IFService", this);
			this.m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckConnection(), 5000L, 5000L);
			return this.client.reset();
		}
		return true;
	}

	public void stop() {
		if (this.m_command != null)
			this.m_command.cancelCommand();
		this.m_command = null;
		if (this.m_ngaConnection != null)
			this.m_ngaConnection.close();
		this.m_ngaConnection = null;

		this.client.Shutdown();
	}

	public void UnregisterComponent(NGAConnection connection, NGAConnection.ReasonLost reason) {
		this.m_ngaConnection = null;
	}

	public void RegisterComponent(NGAConnection connection) {
		this.m_log.debug("Register listener:%s", connection.toString());
		this.m_ngaConnection = connection;
		connection.setCallback(this);
	}

	public void checkForBackup(int arg0) {
	}

	public void ProcessNGAMessage(NGAMessage m_message) {
		this.m_log.debug("Process NGA message: %s", m_message.Command);
		String cmd = m_message.Command;
		if ("TAGGED".equalsIgnoreCase(cmd)) {
			processTagged(m_message);
		} else if ("STARTED".equalsIgnoreCase(cmd)) {
			processStarted(m_message);
		} else if ("STOPPED".equalsIgnoreCase(cmd)) {
			processStopped(m_message);
		} else if ("HELLO".equalsIgnoreCase(cmd))
			processHello(m_message);
		else if ("UPDATE".equalsIgnoreCase(cmd)) {
			this.m_log.debug("Update:%s", m_update);
		}
		if ("EXTENDEDINFO_RESPONSE".equalsIgnoreCase(cmd))
			processExtendedInfoResponse(m_message);
	}
	private void processExtendedInfoResponse(NGAMessage m_message) {
		this.m_log.debug("Received EXTENDEDINFO_RESPONSE");
	}
	private void processHello(NGAMessage message) {
		sendQuery();
		this.m_log.info("Process HELLO message: " + message.getHost());
	}
	private void processStarted(NGAMessage message) {
		this.m_log.debug("Process StartedMessage: %s", message.Command);
		Recording m_recording = new Recording(message.Inum);
		try {
			m_recording.m_extn = message.getExtension();
			m_recording.m_callID = message.getCallRef();
			this.m_log.debugHigh("Extension is:", m_recording.m_extn);
		} catch (Exception e) {
			//this.m_log.error("Exception getting extension: %s", e.getMessage());
			m_recording.m_extn = message.getStringParameterSafe("Extension");
			this.m_log.debugHigh("Defaulting Extension to parameter safe value:%s", m_recording.m_extn);
		}
		try {
			m_recording.m_callID = message.getCallRef();
		} catch (Exception e) {
			//this.m_log.error("Exception getting CallID: %s", e.getMessage());
			m_recording.m_callID = message.getStringParameterSafe("CallID");
			this.m_log.debugHigh("Defaulting CallID to parameter safe value:%s", m_recording.m_callID);
		}
		this.m_log.error("CallID: %s", m_recording.m_callID);
		m_recording.m_tdm = this;
		try {
			CallTracker.add(message.Inum, m_recording);
		} catch (Exception e) {
			System.out.println("Exception adding call to tracker:" + e.getMessage());
		}
	}
	private void processStopped(NGAMessage message) {
		this.m_log.debug("Process StoppedMessage: %s", message.Inum);
		try {
			CallTracker.remove(message.Inum);
		} catch (Exception e) {
			this.m_log.error("Exception removing call from tracker:%s", e.getMessage());
		}
	}
	private void processTagged(NGAMessage message) {
		this.m_log.debug("Process Tagged Message");
		String s_ext = null;
		String s_ddilabel = null;
		String s_lineId = null;
		try {
			s_ext = message.getStringParameterSafe("udf8");
			s_ddilabel = message.getStringParameterSafe("udf5");
			s_lineId = message.getStringParameterSafe("udf10");
		} catch (Exception e) {
			System.out.println("Exception getting udf values:" + e.getMessage());
		}
		Recording rec = CallTracker.getRecording(message.Inum);
		if (null == rec) {
			this.m_log.debugHigh("No recording found in tracker");
			return;
		}
		if (null != s_ext && s_ext.length() > 0) {
			this.m_log.debugHigh("Got DDI:%s, setting extension value.", s_ext);
			rec.m_extn = s_ext;
		}
		if (null != s_ddilabel && s_ddilabel.length() > 0) {
			this.m_log.debugHigh("Got DDILabelId:%s", s_ddilabel);
		}
		if (null != s_lineId && s_lineId.length() > 0) {
			this.m_log.debugHigh("Get lineId:%s", s_lineId);
		}
		// sendTagMessage(message.Inum,
		// "PROPERTY:udf11: VALUE:e-Dialler Call:");

		if (null == message.getParameters())
			return;
		Map<String, String> m_attr = null;
		try {
			m_attr = message.getParameters();
			this.m_log.error("Map contains:%s", m_attr.size());
		} catch (Exception e) {
			this.m_log.error("Error getting call attributes:%s", e.getMessage());
			this.m_log.error("Map contains:%s", m_attr.size());

		}
		rec.addAttributes(m_attr);
		for (Map.Entry<String, String> entry : message.getParameters().entrySet()) {
			System.out.println("pt:" + entry.getKey().toString() + " val:" + entry.getValue().toString());
		}
		System.out.println("Update Tracker for inum:" + message.Inum);
		CallTracker.update(message.Inum, rec);

	}
	private boolean updateTagging(String value) {
		return true;
	}
	public int getMajorVersion() {
		return this.m_ngaConnection == null ? 0 : this.m_ngaConnection.getMajorVersion();
	}
	public void sendTagMessage(String inum, Map<String, String> tagKVs) {
		String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
		String tagString = buildString(tagKVs);
		String cmdId = generateCommand();
		if (tagString.length() > 0) {
			sendMessage(tagMsgVer + " TAGGED CMDID:" + cmdId + ": INUM:" + inum + ": " + tagString + "\033");
			this.m_log.info("TAG CMDID:%s : INUM:%s:%s", cmdId, inum, tagString);
		}
	}
	public void sendTagMessage(String inum, String tag) {
		String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
		String cmdId = generateCommand();
		if (tag.length() > 0) {
			sendMessage(tagMsgVer + " TAG CMDID:" + cmdId + ": MC:: INUM:" + inum + ": " + tag);// +
																								// "\033");
			this.m_log.info("TAG CMDID:%s : INUM:%s:%s", cmdId, inum, tag);
		}
	}

	public void breakCall(String callId, String extn, String inum, eQCEvent m_event) {
		try {
			// BREAK CMDID:891650: MC:27:
			System.out.println("Breaking for device:" + extn);
			System.out.println("Message received is:" + m_event.m_contactevent);
			if ("Connected".equalsIgnoreCase(m_event.m_contactevent)) {

				Recording rec = CallTracker.getRecording(inum);

				if (null == rec) {
					System.out.println("No recording found for break call");
				}
				System.out.println("Got recording");
				Date d_start = new Date(System.currentTimeMillis());
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				System.out.println("Add StartTime:" + sf.format(d_start));
				rec.m_tagDetails.addTags("starttime", sf.format(d_start));
				System.out.println("Add DDI:" + extn);
				rec.m_tagDetails.addTags("DDI", extn);
				System.out.println("Add PhantomDDI:" + extn.substring(1));
				rec.m_tagDetails.addTags("PhantomDDI", extn.substring(1));
				System.out.println("Add Digits:" + m_event.m_digits);
				rec.m_tagDetails.addTags("Digits", m_event.m_digits);
				rec.m_tagDetails.addTags("Direction", "2");
				System.out.println("Add DDILabel:" + rec.getAttribute("UDF5"));
				rec.m_tagDetails.addTags("DDIlabel", rec.getAttribute("UDF5"));
				System.out.println("lineId:" + rec.getAttribute("UDF10"));
				rec.m_tagDetails.addTags("lineId", rec.getAttribute("UDF10"));
				rec.m_tagDetails.addTags("console", rec.getAttribute("UDF6"));
				rec.m_tagDetails.addTags("num_called", m_event.m_attrs.get("num_called"));
				String s_type = rec.getAttribute("UDF6");
				String s_cons = s_type;
				s_cons = s_cons.substring(0, s_cons.length() - 2);
				String s_hsnum = s_type;
				s_hsnum = s_hsnum.substring(s_hsnum.length() - 1);
				int i_st = s_type.length() - 1;
				s_type = s_type.substring(i_st);
				int i_type = 1;
				// if (s_)
				String s_sql = "SELECT TOP 1 VertConfigID FROM VertConfigs WHERE" + " CONSOLE = " + s_cons + " AND HSNUM = " + s_hsnum + " AND verttype = 1" + " ORDER BY ValidFrom DESC";
				System.out.println("Run SQL:" + s_sql);
				ResultSet rs = Runner.m_sql.conQuery(s_sql);
				String s_lineNameId = "";
				String s_DDILabelId = "";
				String s_vertConfigId = "";
				if (rs.next()) {
					System.out.println("VertConfig is:" + rs.getString(1));
					s_vertConfigId = rs.getString(1);
					rec.m_tagDetails.addTags("VertConfigId", rs.getString(1));
				}
				s_sql = "SELECT TOP 1 lineNameId FROM Calls WHERE" + " lineId = " + rec.getAttribute("UDF10") + " AND DDI = " + extn + " ORDER BY starttime DESC";
				System.out.println("Run SQL:" + s_sql);
				rs = Runner.m_sql.conQuery(s_sql);
				if (rs.next()) {
					System.out.println("lineNameId is:" + rs.getInt(1));
					s_lineNameId = rs.getString(1);
					rec.m_tagDetails.addTags("lineNameId", s_lineNameId);
				}

				s_sql = "SELECT TOP 1 id FROM ddi_line_name WHERE" + " label = '" + rec.getAttribute("UDF5") + "'";
				System.out.println("Run SQL:" + s_sql);
				rs = Runner.m_sql.conQuery(s_sql);
				if (rs.next()) {
					System.out.println("DDLabelId is:" + rs.getInt(1));
					s_DDILabelId = rs.getString(1);
					//fix display label to be edial
					s_DDILabelId = "6552";
					rec.m_tagDetails.addTags("DDILabelId", s_DDILabelId);
				}

				// create edialler call
				EDiallerCall eCall = new EDiallerCall();
				eCall.m_extn = m_event.m_device;
				eCall.addTags("starttime", sf.format(d_start));
				eCall.addTags("DDI", extn);
				eCall.addTags("PhantomDDI", extn.substring(1));
				eCall.addTags("Digits", m_event.m_digits);
				eCall.addTags("Direction", "2");
				eCall.addTags("DDIlabel", rec.getAttribute("UDF5"));
				eCall.addTags("lineId", rec.getAttribute("UDF10"));
				eCall.addTags("console", rec.getAttribute("UDF6"));
				eCall.addTags("VertConfigId", s_vertConfigId);
				eCall.addTags("lineNameId", s_lineNameId);
				eCall.addTags("DDILabelId", s_DDILabelId);
				eCall.addTags("num_called", m_event.m_attrs.get("num_called"));
				CallTracker.addEDial(eCall);
				CallTracker.update(inum, rec);
				rec.m_tagDetails.DisplayTagDetails();
			}
			if ("Disconnected".equalsIgnoreCase(m_event.m_contactevent)) {
				// disconnected send updates
				Recording rec = CallTracker.getRecording(inum);
				if (null == rec) {
					System.out.println("No recording found for break call");
				}
				Date d_start = new Date(System.currentTimeMillis());
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				rec.m_tagDetails.addTags("endtime", sf.format(d_start));
				rec.m_tagDetails.DisplayTagDetails();

				String s_sql = " INSERT INTO Calls (starttime,endtime,DDI,PhantomDDI,Digits,direction,DDILabelId,lineId,lineNameId)" + "VALUES(" + "'" + rec.m_tagDetails.getTag("starttime") + "'"
						+ ", '" + rec.m_tagDetails.getTag("endtime") + "'" + ", " + rec.m_tagDetails.getTag("DDI") + " ," + rec.m_tagDetails.getTag("PhantomDDI") + ", "
						+ m_event.m_attrs.get("num_called") + ", 2" + ", " + rec.m_tagDetails.getTag("DDILabelId") + ", " + rec.m_tagDetails.getTag("lineId") + ", "
						+ rec.m_tagDetails.getTag("lineNameId") + ");";
				System.out.println("SQL:" + s_sql);
				int i_callId = Runner.m_sql.updateCalls(s_sql);

				s_sql = "INSERT INTO conns(vertconfigid,starttime,endtime,callid) VALUES(" + rec.m_tagDetails.getTag("VertConfigId") + ", '" + rec.m_tagDetails.getTag("starttime") + "'" + ", '"
						+ rec.m_tagDetails.getTag("endtime") + "'" + ", " + i_callId + ");";
				System.out.println("SQL:" + s_sql);
				i_callId = Runner.m_sql.updateCalls(s_sql);

			}

		} catch (Exception e) {
			m_log.error("Error processing break message: %s", e.getMessage());
		}
	}

	public void sendQuery() {
		String cmdId = generateCommand();
		this.m_log.debug("1 EXTENDEDINFO_QUERY CMDID:%s", cmdId);
		sendMessage("1 EXTENDEDINFO_QUERY CMDID:" + cmdId + ":\033");
	}
	public String buildString(Map<String, String> tagKVs) {
		StringBuilder tagString = new StringBuilder();
		if (tagKVs != null) {
			for (Map.Entry kv : tagKVs.entrySet())
				tagString.append(HexUtils.encode(((String) kv.getKey()).toUpperCase())).append(":").append(HexUtils.encode((String) kv.getValue())).append(": ");
		}
		return tagString.toString();
	}

	private boolean sendMessage(String message) {
		if (this.m_ngaConnection == null)
			return false;
		this.m_ngaConnection.sendMessage(message);
		return true;
	}
	private int reserveCommand(int count) {
		if (count < 1) {
			return 0;
		}
		synchronized (this.m_commandLock) {
			int command = this.m_commandId;
			this.m_commandId += count;
			return command;
		}
	}
	private String generateCommand() {
		return m_hostname + reserveCommand(1);
	}
	private String findChannel(NGAMessage message) {
		String ret = null;
		String cn = message.ChannelRef;
		if (cn.length() == 0) {
			String inum = message.Inum;
			if (inum.length() == 0) {
				this.m_log.debug("Both inum and Channel are empty: %s", message.Command);
			}
		}
		return ret;
	}

	private class CheckConnection implements Runnable {
		private CheckConnection() {
		}
		public void run() {
			// System.out.println("Check connection for recorder");
			if (TdmRole.this.m_ngaConnection != null) {
				// System.out.println("Its not null");
				if (!TdmRole.this.m_ngaConnection.isAlive()) {
					System.out.println("its not alive, reset");
					TdmRole.this.client.reset();
				}
			} else {
				System.out.println("it is null, reset");
				TdmRole.this.client.reset();
			}
		}
	}
}
