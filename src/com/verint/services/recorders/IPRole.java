package com.verint.services.recorders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.verint.services.eQConnect.eQCEvent;
import com.verint.services.model.CallTracker;
import com.verint.services.model.EDiallerCall;
import com.verint.services.model.Recording;
import com.witness.common.NGA.IComponentRegistration;
import com.witness.common.NGA.IProcessNGAMessage;
import com.witness.common.NGA.NGAClient;
import com.witness.common.NGA.NGAConnection;
import com.witness.common.NGA.NGAMessage;
import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;
import com.witness.common.util.HexUtils;
import com.witness.common.wdls.WitLogger;

public class IPRole implements IComponentRegistration, IProcessNGAMessage
{
	private final WitLogger m_log = WitLogger.create("IPRole");
	private String m_hostname;
	private int m_id;
	private int m_port = 1462;
	private NGAConnection m_ngaConnection;
	private NGAClient client = null;
	private TimerWheelCommand m_command;
	private Object m_commandLock = new Object();
	private int m_commandId = 1;
	private int m_update = 1;
	private List<String> m_extn = new ArrayList<String>();


	public IPRole(int id, String hostname) {
		m_log.debugHigh("Creating Role for recorder id:%s",hostname);
		this.m_id = id;
		this.m_hostname = hostname;
		start();
	}
	public boolean start() {
		this.m_log.info("Get connection to Recorder %s on port %s",m_hostname,m_port);
		if (this.client == null) {
			this.client = new NGAClient(m_hostname, m_port, 1, "IF", "IFService", this);
			this.m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckConnection(), 5000L, 5000L);
			return this.client.reset();
		}
		return true;
	}	
	public void stop() {
		if (this.m_command != null)
			this.m_command.cancelCommand();
		    this.m_command = null;
		    if (this.m_ngaConnection != null)
		    	this.m_ngaConnection.close();
		    this.m_ngaConnection = null;

		    this.client.Shutdown();
		}
	public void UnregisterComponent(NGAConnection connection, NGAConnection.ReasonLost reason){
		this.m_log.debugHigh("Connection lost:%s", reason.toString());
		this.m_ngaConnection = null;
	}

	public void RegisterComponent(NGAConnection connection) {
	    this.m_log.debug("Register listener:%s",connection.toString());
	    this.m_ngaConnection = connection;
	    connection.setCallback(this);
	}
	public void checkForBackup(int arg0)
	{
	}
	 public void ProcessNGAMessage(NGAMessage m_message) {
		 this.m_log.debug("Process NGA message: %s",m_message.Command );
		    String cmd = m_message.Command;
		    try{
			    if ("TAGGED".equalsIgnoreCase(cmd)) {
			    	processTagged(m_message);
			    }
			    else if ("STARTED".equalsIgnoreCase(cmd)) {
			    	processStarted(m_message);
			    }
			    else if("STOPPED".equalsIgnoreCase(cmd)) {
			    	processStopped(m_message);
			    }
			    else if ("HELLO".equalsIgnoreCase(cmd))
			    	processHello(m_message);
			    else if ("UPDATE".equalsIgnoreCase(cmd)){
			    	processUpdateMessage(m_message);
			    }
			    if ("EXTENDEDINFO_RESPONSE".equalsIgnoreCase(cmd))
			    	processExtendedInfoResponse(m_message);
			    if ("FALLBACK".equalsIgnoreCase(cmd))
			    	processFallback(m_message);
			    if ("STARTEXTENSIONUPDATE".equalsIgnoreCase(cmd))
			    	processStartExtensionUpdate();		    
		    }
		    catch (Exception e) {
		    	m_log.error("Error processing NGA Message:%s", e.getMessage());
		    }
	 }
	 private void processStartExtensionUpdate() {
		 //m_log.debug("Extension update message received, clearing list");
		 //m_extn.clear();
	 }
	 private void processFallback(NGAMessage message) {
		 //m_log.info("Fallback received");
	 }
	 private void processExtendedInfoResponse(NGAMessage m_message) {
		 //m_log.debug("Received EXTENDEDINFO_RESPONSE");
	 }
	 private void processHello(NGAMessage message) {
		 try {
			 //sendQuery();
		 }
		 catch (Exception e) {
			 m_log.error("Error processing HELLO message: %s", e.getMessage());
		 }
	 }
	 private void processStarted(NGAMessage message) {
		 this.m_log.debug("Process StartedMessage: %s",message.Inum);
		 try {
			 Recording m_recording = new Recording(message.Inum);
			 try {
				 m_recording.m_extn = message.getExtension().substring(4);
				 m_recording.m_callID = message.getCallRef();
			 }
			 catch (Exception e) {
				 //this.m_log.error("Exception getting extension: %s", e.getMessage());
				 m_recording.m_extn = message.getStringParameterSafe("Extension");
				 if (m_recording.m_extn.length() >5) {
					 m_recording.m_extn = m_recording.m_extn.substring(4);
				 }
				 this.m_log.debugHigh("Defaulting Extension to parameter safe value:%s", m_recording.m_extn);
			 }
			 try{
				 m_recording.m_callID = message.getCallRef();
			 } catch (Exception e) {
				 //this.m_log.error("Exception getting CallID: %s", e.getMessage());
				 m_recording.m_callID = message.getStringParameterSafe("CallID");
				 this.m_log.debugHigh("Defaulting CallID to parameter safe value:%s", m_recording.m_callID);
			 }
			 this.m_log.error("CallID: %s", m_recording.m_callID);
			 try {
				 m_recording.m_ip = message.getStringParameter("SRCIP");
				 m_recording.m_ip = m_recording.m_ip.substring(0, m_recording.m_ip.indexOf(":"));
				 this.m_log.debugHigh("SourceIP:%s", m_recording.m_ip);
			 } catch (Exception e) {
				 this.m_log.error("Exception getting source ip address: %s", e.getMessage());
			 }
			 m_recording.m_iprec = this;
			 this.m_log.debugHigh("Add call to tracker for inum:%s", message.Inum);
			 CallTracker.add(message.Inum, m_recording);
		 }
		 catch (Exception e) {
			 this.m_log.error("Exception adding call to tracker:%s", e.getMessage());
		 }
	 }
	 private void processStopped(NGAMessage message) {
		 this.m_log.debug("Process StoppedMessage: %s",message.Inum);
		 try {
			 CallTracker.remove(message.Inum);
		 }
		 catch (Exception e) {
			 this.m_log.error("Exception removing call from tracker:%s", e.getMessage());
		 }
	 }
	 private void processTagged(NGAMessage message) {
		 this.m_log.debug("Process Tagged Message: %s", message.Inum);
		 try {
			 Recording m_recording = CallTracker.getRecording(message.Inum);
			 if (null == m_recording) {
				 this.m_log.debugHigh("No recording found in tracker");
				 return;
			 }
			 if (null == message.getParameters()) return;
			 Map<String, String> m_attr = null;
			 try {
				 m_attr = message.getParameters();
				 this.m_log.error("Map contains:%s", m_attr.size());
			 } catch (Exception e) {
				 this.m_log.error("Error getting call attributes:%s", e.getMessage());
				 this.m_log.error("Map contains:%s", m_attr.size());
				 
			 }
			 m_recording.addAttributes(m_attr);
			 for (Map.Entry<String, String> entry : message.getParameters().entrySet()) {
				 System.out.println("pt:" +entry.getKey().toString() + " val:" +entry.getValue().toString());
				 if (entry.getKey().toString().equalsIgnoreCase("framework")) {
					 System.out.println("Its a framework tag");
					 if (null != (entry.getValue())) {
						 String s_extn = strExtn(entry.getValue());
						 //if (s_extn.length() >1)	m_recording.m_extn = s_extn;
					 }
				 }
			 }
			 System.out.println("Update Tracker for inum:" +message.Inum);
			 CallTracker.update(message.Inum, m_recording);
		 }
		 catch (Exception e) {
			 this.m_log.error("Exception handling tagged message:%s", e.getMessage());
			 e.printStackTrace();
		 }
	 }
	 private static String strExtn(String s_in) {
		 String s_extn = "";
		 if (s_in.contains("Extension")) {
			 int i_start = s_in.indexOf("ion>") +4;
			 int i_stop = s_in.indexOf("</Ext");
			 s_extn = s_in.substring(i_start, i_stop);
			 return s_extn;
		 }
		 if (s_in.contains("udf2")) {
			 int i_start = s_in.indexOf("udf2>") +5;
			 int i_stop = s_in.indexOf("</udf2");
			 s_extn = s_in.substring(i_start, i_stop);
			 return s_extn;
		 }
		 return s_extn;
	 }
	 
	 private void sendKeepTrue(String s_inum) {
		 	String msgString = "1 PRPTAG INUM:" + s_inum + ": KEEP:true: ROLLBACKREQUIRED:false:\033";
		 	m_log.debug("Sending KEEP:true: for this recording:%s",msgString);
		 	sendMessage(msgString);
	 }
	 private void sendKeepFalse(String s_inum) {
		 	String msgString = "1 PRPTAG INUM:" + s_inum + ": KEEP:false: ROLLBACKREQUIRED:true:\033";
		 	m_log.debug("Sending KEEP:false: for this recording:%s",msgString);
		 	sendMessage(msgString);
	 }
	 private void processUpdateMessage(NGAMessage message){
		 //m_log.debug("Process UPDATE message");
		 try {
			 if (null != message.getStringParameter("EXTENSION")) {
				 m_extn.add(message.getStringParameter("EXTENSION"));
				 //m_log.debug("Adding extension: %s", message.getStringParameter("EXTENSION"));
				 return;
			 }
			 m_log.debug("Update extension is null, ignoring");
		 }
		 catch (Exception e) {
			 m_log.error("Error processing Update message: %s", e.getMessage());
		 }
	 }

	 public int getMajorVersion() {
		 return this.m_ngaConnection == null ? 0 : this.m_ngaConnection.getMajorVersion();
	 }
	 public void sendTagMessage(String inum, Map<String, String> tagKVs) {
		 String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
		 String tagString = buildString(tagKVs);
		 String cmdId = generateCommand();
		 if (tagString.length() > 0) {
			 sendMessage(tagMsgVer + " TAG CMDID:" + cmdId + ": INUM:" + inum + ": " + tagString + "\033");
			 this.m_log.info("TAG CMID:%s : INUM:%s:%s",cmdId,inum,tagString);
		 }
	 }
	public void sendIPTagMessage(String inum, Map<String, String> tagKVs) {
		try {
			this.m_log.debug("Send IP Tag Message to inum:" +inum);
			String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
			String tagString = buildString(tagKVs);
			this.m_log.debug("Tag String: " +tagString);
			if (tagString.length() > 0) {
				String msgString = tagMsgVer + " TAG INUM:" + inum + ": TAG:" + tagString + "\033";
				sendMessage(msgString);
				m_log.debug("Sending:%s",msgString);
			}
		}
		catch (Exception e) {
			m_log.error("Error sending IPTAG command: %s", e.getMessage());
		}
	}
	public void sendIPTagMessage(String inum, String tag) {
		try {
			this.m_log.debug("Send IP Tag Message to inum:" +inum);
			String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
			this.m_log.debug("IP Tag String: " +tag);
			if (tag.length() > 0) {
				String msgString = tagMsgVer + " TAG INUM:" + inum + ": XML:" + tag + ":\033";
				sendMessage(msgString);
				m_log.debug("Sending:%s",msgString);
			}
		}
		catch (Exception e) {
			m_log.error("Error sending IPTAG command: %s", e.getMessage());
		}
	}
	public void sendIPExtTagMessage(String callid, String extn) {
		try {
			this.m_log.debug("Send IP ExtTag Message to callid:" +callid);
			String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
			String msgString = tagMsgVer + " EXTTAG CALLID:" + callid + ": " + "EXT:" +extn +":" + "\033";
			m_log.debug("Sending:%s",msgString);
			sendMessage(msgString);
		}
		catch (Exception e) {
			m_log.error("Error sending Tag message: %s", e.getMessage());
		}
	}
	public void breakCall(String callID, String extn, String inum, eQCEvent eQCevent) {
		try {
			this.m_log.debug("Send break for callid:" +callID);
			String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
			String msgString = tagMsgVer + " STOP CALLID:" + callID + ": " + "\033";
			m_log.debug("Sending:%s",msgString);
			sendMessage(msgString);
			msgString = tagMsgVer + " START CALLID:" + callID + ": " + "\033";
			m_log.debug("Sending:%s",msgString);
			sendMessage(msgString);
			Map<String, String> tags = new HashMap<String, String>();
			Map<String, String>attrs = new HashMap<String, String>();
			eQCevent.displayEvent();
			attrs = eQCevent.m_attrs;
			String s_called = attrs.get("num_called");
			//tags.put("udf5", "e-Dialler Call");
			tags.put("udf8", s_called);
			tags.put("stitchid", "");
			tags.put("origstitchid", inum);
			System.out.println("BreakCall adding details:" +inum);
			EDiallerCall m_edial = new EDiallerCall();
			m_edial.m_callid = callID;
			m_edial.m_extn = extn;
			System.out.println("Setting Digits to:" +attrs.get("num_called"));
			m_edial.m_digits = attrs.get("num_called");
			System.out.println("Created eDialler call:" +callID +" ; " +extn);
			CallTracker.addEDial(m_edial);
			CallTracker.addTagQ(extn, tags);
		}
		catch (Exception e) {
			m_log.error("Error sending Tag message: %s", e.getMessage());
		}
	}
	public void sendQuery() {
		try {
			String cmdId = generateCommand();
			this.m_log.debug("1 EXTENDEDINFO_QUERY CMDID:%s",cmdId);
			//sendMessage("1 EXTENDEDINFO_QUERY CMDID:" +cmdId+":\033");
			sendMessage("1 REQUESTEXTENSIONUPDATE TYPE:FULL: CMDID:" +cmdId+":\033");
		}
		catch (Exception e) {
			m_log.error("Error sending query for 'REQUESTEXTENSIONUPDATE' : %s", e.getMessage());
		}
	}
	 public String buildString(Map<String, String> tagKVs) {
		 StringBuilder tagString = new StringBuilder();
		 if (tagKVs != null) {
			 for (Map.Entry<String, String> kv : tagKVs.entrySet())
				 tagString.append(HexUtils.encode(((String)kv.getKey()).toUpperCase())).append(":").append(HexUtils.encode((String)kv.getValue())).append(": ");
		 }
		 return tagString.toString();
	 }

	 private boolean sendMessage(String message) {
		 if (this.m_ngaConnection == null) 
			 return false;
		 this.m_ngaConnection.sendMessage(message);
		 return true;
	 }
	 private int reserveCommand(int count) {
		 if (count < 1) {
			 return 0;
		 }
		 synchronized (this.m_commandLock) {
			 int command = this.m_commandId;
			 this.m_commandId += count;
			 return command;
		 }
	}
	private String generateCommand() {
		return m_hostname + reserveCommand(1);
	}
	private String findChannel(NGAMessage message) {
		String ret = null;
		String cn = message.ChannelRef;
		if (cn.length() ==0) {
			String inum = message.Inum;
			if (inum.length() ==0) {
				this.m_log.debug("Both inum and Channel are empty: %s",message.Command);
			}
		}
		return ret;
	}
	private String getExtension(String s_callRef) {
		String s_ret = "";
		
		return s_ret;
	}

	private class CheckConnection implements Runnable {
		private CheckConnection() {
			}
		public void run() {
			//System.out.println("Check connection for recorder");
			if (IPRole.this.m_ngaConnection != null) {
				if (!IPRole.this.m_ngaConnection.isAlive()) {
					//System.out.println("its not alive, reset");
					IPRole.this.client.reset();
				}
			} 
			else {
				//System.out.println("it is null, reset");
				IPRole.this.client.reset();
			}
		}
	}
}

