package com.verint.services.recorders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class RecorderManager{
	private static Map<Integer, BaseRecorder> m_recorders = new HashMap();
	
	public RecorderManager() {	}
	
	public boolean addRecorder(int id, String hostname, String serialNo, List<String> roles, Map<String,String> rec_settings) {
		System.out.println("Recorder Manager");
		BaseRecorder recorder = m_recorders.get(id);
		if (null == recorder){
			System.out.println("Creating new recorder in recorder manager:" + hostname);
			recorder = new BaseRecorder(id, hostname, serialNo, roles, rec_settings);
		}
		m_recorders.remove(id);
		m_recorders.put(id, recorder);
		return true;
	}
	public BaseRecorder getRecorder(int id) {
		return m_recorders.get(id);
	}
	public static String GetRecorderDetails(String s_serial) {
		String s_ret = "";
		for (int i : m_recorders.keySet()) {
			BaseRecorder recorder = m_recorders.get(i);
			if (recorder.m_serialNo.equalsIgnoreCase(s_serial)){
				s_ret = recorder.m_hostname + " with serial no#:" +recorder.m_serialNo;
				return s_ret;
			}
		}
		return s_ret;
	}
}
