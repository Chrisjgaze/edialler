package com.verint.services.tcp;

import java.net.*;
import java.io.*;

public class SktServerThread extends Thread {

	    private Socket socket = null;

	    public SktServerThread(Socket socket) {
		this.socket = socket;
	    }

	    public void run() {
		try {
		    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
		    BufferedReader in = new BufferedReader(
					    new InputStreamReader(
					    socket.getInputStream()));

		    String inputLine;
		    while ((inputLine = in.readLine()) != null) {
				if (inputLine.equals("Bye"))
				    break;
			}
		    out.close();
		    in.close();
		    socket.close();

		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	}

