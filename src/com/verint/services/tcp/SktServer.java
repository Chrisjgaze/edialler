package com.verint.services.tcp;

import java.io.IOException;
import java.net.ServerSocket;


	public class SktServer extends Thread{
		public void run() {

		}

	    public SktServer() throws IOException {
	        ServerSocket serverSocket = null;
	        boolean listening = true;
	        try {
	            serverSocket = new ServerSocket(4444);
	        } 
	        catch (IOException e) {
	            System.exit(-1);
	        }

	        while (listening)
	        	new SktServerThread(serverSocket.accept()).start();
	        serverSocket.close();
	    }
	}
